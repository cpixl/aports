# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=k9s
_pkgname=github.com/derailed/k9s
pkgver=0.21.7
pkgrel=0
_commit=cc0e8e8 # git rev-parse --short HEAD
_date=2020-09-06T17:07:54UTC # date -u -d @$(date +%s) +%FT%T%Z
pkgdesc="Kubernetes TUI"
url="https://k9scli.io"
arch="all !x86 !mips64" # tests fail on x86 for some reason, missing go on mips
license="Apache-2.0"
makedepends="go"
options="net chmod-clean"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/derailed/k9s/archive/v$pkgver.tar.gz
	01-fix_action.patch
	"

export GOPATH="$srcdir/go"
export GOCACHE="$srcdir/go-build"
export GOTMPDIR="$srcdir"

build() {
	local ldflags="-w -s -X $_pkgname/cmd.version=$pkgver -X $_pkgname/cmd.commit=$_commit -X $_pkgname/cmd.date=$_date"
	go build -ldflags "$ldflags" -a -tags netgo -o execs/$pkgname main.go
}

check() {
	go test ./...
}

package() {
	install -Dm755 execs/$pkgname "$pkgdir"/usr/bin/$pkgname
}

sha512sums="ea1ce0c84013ec9d46e98b9fa53be1e889aab3630dbd34c06f0d0ff45c30e89b5657182a0490a5832f2a91e17a1401c42bf3edc7f679c53acfcc494fbe1bf141  k9s-0.21.7.tar.gz
9c2e637336aaf88dfc9202ae89b14979d6cb99c0a4c9945e4ba75077dc99d3534b2c8cf0f6d4d818fc29c476ab3c6253e873c11ce3aa1db11777a698cd4a8a6a  01-fix_action.patch"
